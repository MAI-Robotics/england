'''
main file for the Brainboard (Rasperry Pi 4)
currently its purpose is to control the camera and use the serial connection to the ruggeduino to supply it
with necessary information for
'''
import serial
import struct
import math
from sr.robot3 import *

RUGGEDUINO_ID = "7523031383335161C051"



# constants

PORT: str = '/dev/ttyACM0' #the port the arduino is connected to (might be '/dev/ttyUSB0')
BAUDRATE: int = 115200
TEST_MARKER: int = 27 #wall markers id range [0, 27]; token id: > 27

# lambdas
rad_to_degree = lambda rad: (rad*180)/math.pi

# variables
requested_marker = 0
R = Robot(ignored_ruggeduinos=["7523031383335161C051"])

# main section
serial1 = serial.Serial(port=PORT, baudrate=BAUDRATE, timeout=1)
serial1.reset_input_buffer()

R.camera.save(R.usbkey / "initial-view.png")
print("er hat gestartet")
# main loop
while True:
    print("in while")
    if serial1.in_waiting > 0: # if there are requests by arduino waiting
        print("received")
        try:
            received_bytes = serial1.read(2) # 2 bytes = two digit number represented as a string containing ascii chars
            print(repr(received_bytes))
            requested_marker = int.from_bytes(received_bytes, byteorder='big')
            print(requested_marker)
        except Exception as e:
            print(e)
            
        markers = R.camera.see()
        print(markers)
        print("###################################")
        for marker in markers:
            
            if marker.id == requested_marker:
                # send arduino required information
                serial1.write(f'{marker.distance},{round(rad_to_degree(marker.spherical.rot_y))}\n'.encode('utf-8'))
                print(f'{marker.distance},{round(rad_to_degree(marker.spherical.rot_y))}')
                R.kch.leds[UserLED.B] = Colour.MAGENTA