import math
from sr.robot3 import *

R = Robot()

TOLERANCE_DEG = 2

motor_l = R.motor_board.motors[0]
motor_r = R.motor_board.motors[1]

rad_to_degrees = lambda x: x * 180 / math.pi

def stop():
    motor_l.power = 0
    motor_r.power = 0

def get_marker_from_list(markers, marker_id):
    
    for marker in markers:
        if marker.id == marker_id:
            return marker

def turn(direction, time, power):
    if direction == 'l':
        motor_r.power = power / 2
        motor_l.power = -power / 2
    elif direction == 'r':
        motor_l.power = power / 2
        motor_r.power = -power / 2

    R.sleep(time)
    stop()

def get_marker_visible(marker_id):
    visible_ids = R.camera.see_ids()
    if marker_id in visible_ids:
        return True
    else:
        return False

def get_center_marker():
    markers = R.camera.see()

    temp_marker = markers[0]

    for marker in markers:
        print(marker)
        if abs(marker.spherical.rot_y) < abs(temp_marker.spherical.rot_y):
            temp_marker = marker

    return temp_marker

def get_closest_marker():
    markers = R.camera.see()

    temp_marker = markers[0]

    for marker in markers:
        if marker.distance < temp_marker.distance:
            temp_marker = marker

    return temp_marker

def center_marker(marker_id):
    while get_marker_visible(marker_id) != True:
        turn('r', 0.05, 0.2)
    
    
    marker = get_marker_from_list(R.camera.see(), marker_id)

    while rad_to_degrees(marker.spherical.rot_y) > TOLERANCE_DEG:
        marker = get_marker_from_list(R.camera.see(), marker_id)

        turn('r', 0.01, 0.1)
        
    while rad_to_degrees(marker.spherical.rot_y) < -TOLERANCE_DEG:
        marker = get_marker_from_list(R.camera.see(), marker_id)

        turn('l', 0.01, 0.1)


def go_to_center_marker(distance, power, interval):

    marker = get_center_marker()
    status = 'C'

    while marker.distance > distance:

        marker = get_center_marker()
        print(rad_to_degrees(marker.spherical.rot_y))

        if rad_to_degrees(marker.spherical.rot_y) > TOLERANCE_DEG:
            motor_l.power = power + 0.1
            status = 'L'
        elif rad_to_degrees(marker.spherical.rot_y) < -TOLERANCE_DEG:
            motor_r.power = power + 0.1
            status = 'R'
        else:
            motor_l.power = power
            motor_r.power = power
            status = 'C'

        R.sleep(interval)

        print(status)
        print(marker.distance)

    print(marker.distance)
    stop()

def go_to_marker(marker_id, distance, power, interval):
    
    marker = get_marker_from_list(R.camera.see(), marker_id)
    status = 'C'

    while marker.distance > distance:

        marker = get_center_marker()
        print(rad_to_degrees(marker.spherical.rot_y))

        if rad_to_degrees(marker.spherical.rot_y) > TOLERANCE_DEG:
            motor_l.power = power + 0.1
            status = 'L'
        elif rad_to_degrees(marker.spherical.rot_y) < -TOLERANCE_DEG:
            motor_r.power = power + 0.1
            status = 'R'
        else:
            motor_l.power = power
            motor_r.power = power
            status = 'C'

        R.sleep(interval)

        print(status)
        print(marker.distance)

    print(marker.distance)
    stop()

go_to_center_marker(1200, 0.4, 0.05)

center_marker(10)

motor_l.power = 0.5
motor_r.power = 0.5

while R.ruggeduino.pins[A3].analogue_read() > 0.3:
    R.sleep(0.02)

R.sleep(0.5)

stop()

while R.ruggeduino.pins[A4].analogue_read() > 0.56:
    print(R.ruggeduino.pins[A4].analogue_read())
    turn('r', 0.05, 0.1)

print(R.ruggeduino.pins[A4].analogue_read())

turn('r', 0.3, 0.1)

go_to_marker(get_closest_marker().id, 170, 0.3, 0.05)

motor_l.power = 0.3
motor_r.power = 0.3

R.sleep(0.3)

stop()

R.servo_board.servos[0].position = 0
R.servo_board.servos[1].position = 0

R.sleep(0.3)

motor_r.power = -0.3
motor_l.power = -0.5

R.sleep(4)

stop()