#define FRONT_TRIG 6
#define FRONT_ECHO 5

#define LEFT_TRIG 10
#define LEFT_ECHO 11

#define RIGHT_TRIG 3 
#define RIGHT_ECHO 2

#define BACK_TRIG 8
#define BACK_ECHO 9

#define emergencyPinFront 13
#define emergencyPinRight 7
#define emergencyPinLeft 12
#define emergencyPinBack 4

#define selectPin A5
