#include "pins.h"
#include <Arduino.h>
#include <NewPing.h>

#define NUM_US 4
#define NUM_US 4
#define MAX_DISTANCE 300
#define emergencyDistance 30        // distance for the bot to detect and react to obstacles (in cm)
#define alternativeDistance 3       // distance for the bot to grab tokens or to cling to a wall (in cm)
#define emergencyDistance 30        // distance for the bot to detect and react to obstacles (in cm)
#define alternativeDistance 3       // distance for the bot to grab tokens or to cling to a wall (in cm) test

NewPing usSensorFront(FRONT_TRIG, FRONT_ECHO, MAX_DISTANCE);
NewPing usSensorBack(BACK_TRIG, BACK_ECHO, MAX_DISTANCE);
NewPing usSensorRight(RIGHT_TRIG, RIGHT_ECHO, MAX_DISTANCE);
NewPing usSensorLeft(LEFT_TRIG, LEFT_ECHO, MAX_DISTANCE);

float usFilteredValues[NUM_US];

int selectedDistance;

void setup(){
    Serial.begin(9600); 

    //pinModes
    pinMode(FRONT_TRIG, OUTPUT);
    pinMode(FRONT_ECHO, INPUT);

    pinMode(BACK_TRIG, OUTPUT);
    pinMode(BACK_ECHO, INPUT);

    pinMode(RIGHT_TRIG, OUTPUT);
    pinMode(RIGHT_ECHO, INPUT);

    pinMode(LEFT_TRIG, OUTPUT);
    pinMode(LEFT_ECHO, INPUT);

    pinMode(emergencyPinFront, OUTPUT);
    pinMode(emergencyPinRight, OUTPUT);    
    pinMode(emergencyPinLeft, OUTPUT);
    pinMode(emergencyPinBack, OUTPUT);

    pinMode(selectPin, INPUT);

}

void selectMode() {
    //select the distance mode the ruggeduino needs
    if(digitalRead(selectPin) == HIGH) {
      selectedDistance = alternativeDistance;   //distance to grab a token in front of the bot
    } else {
      selectedDistance = emergencyDistance;     //distance to detect obstacles
    }
}

int filter(int value, float* lastValue){
    float a = 0.3;

    // Serial.print(value);
    // Serial.print(",");

    float filteredValue = ((1-a)*(*lastValue)) + (a*value);
    // float filteredValue = value;
    *lastValue = filteredValue;

    // Serial.println(filteredValue);

    return filteredValue;

}

void readUS(){
    // read and filter us sensors
    usFilteredValues[0] = filter(usSensorFront.ping_cm(), &usFilteredValues[0]);
    Serial.println(usFilteredValues[0]);
    delay(10);

    usFilteredValues[1] = filter(usSensorLeft.ping_cm(), &usFilteredValues[1]);
    Serial.println(usFilteredValues[1]);
    delay(10);

    usFilteredValues[2] = filter(usSensorRight.ping_cm(), &usFilteredValues[2]);
    Serial.println(usFilteredValues[2]);
    delay(10);
    
    usFilteredValues[3] = filter(usSensorBack.ping_cm(), &usFilteredValues[3]);
    Serial.println(usFilteredValues[3]);
    delay(10);
}
 
void communicate() {

    Serial.print("front:  ");
    if (usFilteredValues[0] < selectedDistance && usFilteredValues[0] != 0){
      Serial.println("zu nah");
      digitalWrite(emergencyPinFront, HIGH);
    } 
    else {
      Serial.println("alles gut");
      digitalWrite(emergencyPinFront, LOW);
    }
    
    delay(20);

    Serial.print("left:  ");
    if (usFilteredValues[1] < selectedDistance && usFilteredValues[1] != 0){
      Serial.println("zu nah");
      digitalWrite(emergencyPinRight, HIGH);
    }
    else {
      Serial.println("alles gut");
      digitalWrite(emergencyPinRight, LOW);
    }

    delay(20);

    Serial.print("right:  ");
    if (usFilteredValues[2] < selectedDistance && usFilteredValues[2] != 0){
      Serial.println("zu nah");
      digitalWrite(emergencyPinLeft, HIGH);
    }
    else {
      Serial.println("alles gut");
      digitalWrite(emergencyPinLeft, LOW);
    }

    delay(20);

    Serial.print("back:  ");
    if (usFilteredValues[3] < selectedDistance && usFilteredValues[3] != 0){
      Serial.println("zu nah");
      digitalWrite(emergencyPinBack, HIGH);
    }
    else {
      Serial.println("alles gut");
      digitalWrite(emergencyPinBack, LOW);
    }
    
    delay(200);

    Serial.println("########################################################");
}

void loop(){

    selectMode(); 
    readUS();
    communicate();

    //Serial.println(usSensorFront.ping_cm());
    //delay(100);
}