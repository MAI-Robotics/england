int tokenDistance = 0;
int tokenRotation = 0;

int serialValue;
String dataString;

byte data[128];

void serialSetup(int baud) {
  Serial.begin(baud);
}

void serialDecode() {
  if (Serial.available() > 0) {
    byte data[128]; // Create a byte array to store the incoming data
    int length = Serial.readBytes(data, sizeof(data)); // Read the incoming data into the byte array
    if (length > 0) { // If data was received
      dataString = String((char*)data); // Create a string from the byte array
    }
  }
}

void receiveInfo() {
  serialDecode();
  dataString.trim(); // Remove any leading or trailing whitespace characters
  int commaIndex = dataString.indexOf(','); // Find the index of the comma in the input string
  if (commaIndex != -1) { // If a comma is found
    String distanceString = dataString.substring(0, commaIndex); // Extract the distance string
    String rotationString = dataString.substring(commaIndex + 1); // Extract the rotation string
    tokenDistance = distanceString.toInt(); // Convert the distance string to an integer and store it in the distance variable
    tokenRotation = rotationString.toInt(); // Convert the rotation string to an integer and store it in the rotation variable
  }

  delay(50);
}
/**
@param markerID has to be an int in range [0, 27] and will be automatically converted to a TWO digit markerID if not already
example: 7 -> 07
*/
void sendID(int markerID) {
  byte data[2];     // create an array to hold the binary data
  if (markerID < 10) {
    data[0] = 0x00;  // add a leading zero byte
    data[1] = markerID & 0xFF;  // extract the low byte
  } else {
    data[0] = (markerID >> 8) & 0xFF;  // extract the high byte
    data[1] = markerID & 0xFF;         // extract the low byte
  }
  Serial.write(data, 2);  // send the binary data over serial

  delay(50);
}


