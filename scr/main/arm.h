/* WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG:

Für die Benutzung der Methode:
  // bei dem Wert 0 steht der Arm senkrecht nach oben
  // bei dem Wert -90 steht der Arm nach hinten 
  // bei dem Wert 90 steht der Arm nach vorne
*/

#include <Servo.h>

Servo servoLinks;
Servo servoRechts;


void tiltArm (unsigned int degree){
  int servoRechtsDegree = 90;  
  int servoLinksDegree = 90;
  servoRechtsDegree = 90 - degree;
  servoLinksDegree = 90 + degree;

  servoLinks.write(servoLinksDegree);
  servoRechts.write(servoRechtsDegree);
}
void hoch()
{
  servoLinks.write(130);
  servoRechts.write(50); 
  delay(700);
  servoLinks.write(60);
  servoRechts.write(120);  
}
void runter()
{
  servoLinks.write(167);
  servoRechts.write(13);  
}
void halbrunter()
{
  servoLinks.write(147);
  servoRechts.write(33);  
}
void ausrichtenNullGrad()
{
  servoLinks.write(180);
  servoRechts.write(0);  
}