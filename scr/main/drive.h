
/* WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG:
  nicht diese Methoden verwenden!

  Methoden aus us_drive.h verwenden!

*/

const int tPerStep = 500;

const int tPerStepFast = 200;

void driveLinear(unsigned int steps, bool dir){         
    digitalWrite(LEFT_DIR, dir ? 1 : 0);
    digitalWrite(RIGHT_DIR, dir ? 0 : 1);


    for (unsigned int i = 0; i < steps; i++)
    {
        digitalWrite(LEFT_STEP, HIGH);
        digitalWrite(RIGHT_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(LEFT_STEP, LOW);
        digitalWrite(RIGHT_STEP, LOW);
        delayMicroseconds(tPerStep);
    }

}

void driveLinearFast(unsigned int steps, bool dir){         
    digitalWrite(LEFT_DIR, dir ? 1 : 0);
    digitalWrite(RIGHT_DIR, dir ? 0 : 1);

    delay(10);
    
    for (unsigned int i = 0; i < steps; i++)
    {
        int a = 0;
        if (i < 300 && i <= steps / 2)           //acceleration section
        {
            a = tPerStepFast * 0.002 * (400 - i);
        }

        if (steps - i < 300 && i > steps / 2)    //deceleration section
        {
            a = tPerStepFast * 0.002 * (400 - (steps - i));
        }

        digitalWrite(LEFT_STEP, HIGH);
        digitalWrite(RIGHT_STEP, HIGH);
        delayMicroseconds(tPerStepFast + a);
        digitalWrite(LEFT_STEP, LOW);
        digitalWrite(RIGHT_STEP, LOW);
        delayMicroseconds(tPerStepFast + a);
    }

}

void driveSidewards(unsigned int steps, bool dir){
    digitalWrite(FRONT_DIR, dir ? 0 : 1);
    digitalWrite(BACK_DIR, dir ? 1 : 0);


    for (unsigned int i = 0; i < steps; i++)
    {
        digitalWrite(FRONT_STEP, HIGH);
        digitalWrite(BACK_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(FRONT_STEP, LOW);
        digitalWrite(BACK_STEP, LOW);
        delayMicroseconds(tPerStep);
    }
}

void turn(unsigned int steps, bool dir){
    digitalWrite(LEFT_DIR, dir ? 0 : 1);
    digitalWrite(RIGHT_DIR, dir ? 0 : 1);
    digitalWrite(FRONT_DIR, dir ? 0 : 1);
    digitalWrite(BACK_DIR, dir ? 0 : 1);

    for (unsigned int i = 0; i < steps; i++)
    {
        digitalWrite(FRONT_STEP, HIGH);
        digitalWrite(BACK_STEP, HIGH);
        digitalWrite(LEFT_STEP, HIGH);
        digitalWrite(RIGHT_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(FRONT_STEP, LOW);
        digitalWrite(BACK_STEP, LOW);
        digitalWrite(LEFT_STEP, LOW);
        digitalWrite(RIGHT_STEP, LOW);
        delayMicroseconds(tPerStep);
    }
}

// Das ist zwar über nice aber brauchen wir nicht wirklich (Ist auch mit nicht Ultraschallsensoren programmiert)
// brauchen wir

// umgeschrieben am 17.03.
// dir: true: vorwärts / false: rückwärts
// dir1: true: rechts / false: links
driveOmni(unsigned int steps, bool dir, bool dir1){
    digitalWrite(LEFT_DIR, dir ? 1 : 0);
    digitalWrite(RIGHT_DIR, dir ? 0 : 1);
    digitalWrite(FRONT_DIR, dir1 ? 1 : 0);
    digitalWrite(BACK_DIR, dir1 ? 0 : 1);
    
    delay(10);

    for (unsigned int i = 0; i < steps; i++)
    {
        digitalWrite(LEFT_STEP, HIGH);
        digitalWrite(RIGHT_STEP, HIGH);
        digitalWrite(FRONT_STEP, HIGH);
        digitalWrite(BACK_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(LEFT_STEP, LOW);
        digitalWrite(RIGHT_STEP, LOW);
        digitalWrite(FRONT_STEP, LOW);
        digitalWrite(BACK_STEP, LOW);
        delayMicroseconds(tPerStep);
    }
    delay(10);
}

