//define stepper pins (red us mount is front)
#define LEFT_DIR 2
#define LEFT_STEP 3

#define RIGHT_DIR 6
#define RIGHT_STEP 7

#define FRONT_DIR 4
#define FRONT_STEP 5

#define BACK_DIR 8
#define BACK_STEP 9

//define arm pins
#define PinServoLinks 10
#define PinServoRechts 11

//define emergency Pins
#define emergencyPinFront A3
#define emergencyPinRight A4
#define emergencyPinLeft A1
#define emergencyPinBack A2

#define DISTANCE_CONTROL 12