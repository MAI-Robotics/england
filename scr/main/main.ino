#include <Arduino.h>
#include <NewPing.h>

#include "pins.h"
#include "drive.h"
#include "arm.h"
#include "logic.h"
#include "us_drive.h"
#include "drive_programs.h"
#include "serial_connection.h"


void setup(){
  //
  // SETUP
  //

    Serial.begin(115200);

    pinMode(LEFT_DIR, OUTPUT);
    pinMode(LEFT_STEP, OUTPUT);
    pinMode(RIGHT_DIR, OUTPUT);
    pinMode(RIGHT_STEP, OUTPUT);
    pinMode(FRONT_DIR, OUTPUT);
    pinMode(FRONT_STEP, OUTPUT);
    pinMode(BACK_DIR, OUTPUT);
    pinMode(BACK_STEP, OUTPUT);
    pinMode(DISTANCE_CONTROL, OUTPUT);
    servoLinks.attach(PinServoLinks);
    servoRechts.attach(PinServoRechts);

  hoch();
  delay(1000);

  // bool erfolgreich = false;
  // while (!erfolgreich) {
  //   sendID(73);
  //   receiveInfo();
  //   if (tokenDistance != 0 && tokenRotation != 0) {
  //     erfolgreich = true;
  //     alignByMarker(tokenDistance, tokenRotation, 0, 500);

  //     tokenDistance = 0;
  //     tokenRotation = 0;
  //   }
  // }

  //
  // LOGIK
  //
  /*
  driveLinearTested(mmToSteps(1700), true, false);
  delay(500);
  turnTested(mmToSteps(axleCircumference / 4), false);
  delay(500);
  driveLinearTested(3000, true, false);
  delay(500);
  driveLinearTested(300, false, false);
  delay(500);
  driveSidewardsTested(3000, false, false);
  delay(500);
  driveLinearTested(1000, true, false);
  delay(500);
  
  pullCubeOver();
  
  tiltArm(-90);
  delay(3000); 
  tiltArm(90);
  delay(3000);
  driveLinearTested(mmToSteps(1000), false, false);
  delay(1500);
  tiltArm(0);
  driveLinearTested(mmToSteps(700), true, false);
  tiltArm(90);
  driveLinearTested(mmToSteps(1000), false, false);
  */
  // delay(5000);
  // while (tokenDistance == 0) {
  //   sendID(73);
  //   receiveInfo();
  // }
  // tiltArm(-30);
  // delay(300);
  // driveLinearTested(mmToSteps(tokenDistance), true, false);
  // delay(500);
  // turnTested(turnAngle, false);
  
  // delay(40000);
  
  // while (tokenDistance == 0)
  // {
  //   sendID(4);
  //   delay(100);
  //   receiveInfo();
  //   delay(100);
  // }
  // tiltArm(-5);
  
  // turnTested(tokenRotation, (tokenRotation > 0) ? true : false);

  // driveLinearTested(mmToSteps(tokenDistance), true, false);

  //turn test
  // driveLinearTested(3000, true, true);
  // delay(500);
  // turnTested(360, true);
  // delay(500);
  // driveLinearTested(3000, true, false);

  // hardcode: würfel in zone 1 und 3
  // cube1();
  // cube3Right(false);
  //saveCube();
  strategie();
  //driveLinearFast(mmToSteps(1000), true);

}

void loop(){

  /* driveLinearTested(3000, true, true);
  delay(500);
  driveSidewardsTested(4000, false, true);
  delay(500);
  driveSidewardsTested(4000, true, true);
  delay(500);
  turn(2000, true);
  turn(2000, false);
  delay(500);
  driveLinearTested(3000, false, true);
  delay(500);
  */

  // tiltArm(80);
  // delay(2000);
  // tiltArm(-90);
  // driveSidewardsTested(1500, true, false);
  // delay(2000);
  
  // delay(3000);

  //driveLinearTested(3000, true, true);

  //driveLinearTested(200, true, false);

  
  //tiltArm(-90)
  
  
}
