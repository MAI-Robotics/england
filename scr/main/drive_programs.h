// zieht den würfel über die wand
// startet in der ecke gegenüber dem würfel
// hört auf nach zurückfahren
#include <math.h>

void signal() {
  tiltArm(5);
  delay(250);
  tiltArm(0);
  delay(250);
}



void saveCube() {
  // Nach vorne an die Wand + Token fahren und ausrichten
  driveLinearFast(mmToSteps(600), true);
  delay(20000);
  driveLinearFast(mmToSteps(600), true);
  delay(500);
  driveLinearTested(mmToSteps(500), true, false, false);
  delay(500);

  //Der Arm umarmt den Token
  runter();
  delay(1500);

  //Der Bot fährt mit dem Token Weg
  driveLinearFast(mmToSteps(800), false);
  delay(500);

  //Der Bot dreht den Token zur Seite
  turn(2000, true);
  delay(500);

  //Der Bot lässt den Token los
  hoch();
  delay(1500);

  //Der Bot dreht zurück
  turn(2000, false);
  delay(500);

  //Der Bot fährt zum ersten checkpoint
  driveLinearFast(mmToSteps(400), true);
  delay(500);
  driveLinearTested(mmToSteps(300), true, false, false);
  delay(500);
}

//Fahrt zum dritten Token
void thirdCube()
{
  //Rückwärtsfahrt hinter das Kreuz
  driveLinearFast(mmToSteps(500), false);
  delay(500);

  //auf Vorwärts drehen
  turn(1400, true);
  delay(500);

  //Vorwärts in gegnerischen Berreich fahren
  driveLinearFast(mmToSteps(1100), true);
  delay(500);

  //Drehen parallel zum Kreuz
  turn(1200, false);
  delay(500);

  //Fahrt am Kreuz vorbei
  driveLinearFast(mmToSteps(1000), true);
  delay(500);

  //Drehen zum Kreuz zum ausrichten
  turn(1300, false);
  delay(500);

  //Fahrt an das Kreuz zum ausrichte
  driveLinearTested(mmToSteps(1000), true, false, false);
  delay(500);
  
  //Rückwärtsfahren vom Kreuz weg
  driveLinearTested(mmToSteps(240), false, false, false);
  delay(500);

  //Drehen zum Token
  turn(1300, true);
  delay(500);

  //Token einsammeln
  driveLinearTested(mmToSteps(800), true, false, false);
  delay(500);
  runter();
  delay(1500);

  //Rückfahrt in die eigene Zone
  driveLinearFast(mmToSteps(1100), false);
  delay(500);
  turn(1300, true);
  delay(500);
  driveLinearTested(mmToSteps(1200), false, false, true);
  delay(500);
  
}

void cubeHolen(bool cubeGesichert)
{
  //Fahrt zum ersten Checkpoint, falls der ersten Cube nicht gesichert wurde
  if (!cubeGesichert) 
  {
      // Nach vorne an die Wand + Token fahren und ausrichten
    driveLinearFast(mmToSteps(1800), true);
    delay(500);
    driveLinearTested(mmToSteps(500), true, false, false);
    delay(500);
    driveLinearTested(mmToSteps(100), false, false, false);
    delay(500);
  }
  //Bot dreht zum Token der Gegner
  turn(1200, false);
  delay(500);

  //Bot fährt zum Gegner-Token
  driveLinearTested(mmToSteps(500), true, false, false);
  delay(500);

  //Bot schiebt Token das erste mal drüber
  halbrunter();
  delay(1500);
  driveLinearTested(mmToSteps(700), false, false, false);
  delay(500);
  hoch();
  delay(1500);

  //Bot fährt vor und sammelt den Token ein zweites mal ein
  driveLinearTested(mmToSteps(300), true, false, false);
  delay(500);
  halbrunter();
  delay(1500);
  driveLinearTested(mmToSteps(700), false, false, false);
  delay(500);
  runter();
  delay(1500);
  hoch();
  delay(500);
  hoch();
  delay(1500);
}

void blockieren()
{
  //Rückwärtsfahrt hinter das Kreuz
  driveLinearTested(mmToSteps(500), false, false, true);

  //auf Vorwärts drehen
  turn(1400, true);
  delay(500);

  //Vorwärts in gegnerischen Berreich fahren zum blockieren
  driveLinearTested(mmToSteps(100), true, false, true);
  delay(500);
}

void strategie()
{
  bool cubeSichern = true;
  bool drittenTokenHolen = true;
  
  if (cubeSichern) {saveCube();}
  cubeHolen(cubeSichern);
  if (cubeSichern) {thirdCube();}
  else{blockieren();}

}

void lookForMarker() {

}

int rad_to_degrees(double rad) {
  return round((rad*180)/M_PI);
}


/**
calculates the coords needed for the method below
*/

int calculate_bot_x(int CurrentDistance, int CurrentRotation) {
  int botX = round(sin(CurrentRotation) * CurrentDistance);
  return botX;
}

int calculate_bot_y(int CurrentDistance, int CurrentRotation) {
  int botY = round(cos(CurrentRotation) * CurrentDistance);
  return botY;
}

/**
targetX and targetY describe the position of the bot inside an imaginary cartesian coordinate
system around the target marker where the marker itself is at position (0,0).
*/



void omniDriveTest() {
  driveOmni(3000, true, true);
  driveOmni(3000, true, false);
  driveOmni(3000, false, false);
  driveOmni(3000, false, true);
}