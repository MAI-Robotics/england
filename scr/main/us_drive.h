
/* WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG WICHTIG:

Für die Benutzung der Methoden:
   steps ist die Weite die gefahren werden soll
   dir ist eine bool die die zu fahrende Richtung angibt
   activated is eine bool die angibt ob die U ltraschallsensoren ausgelesen werden sollen
*/

// gibt an nach wie vielen Steps wieder, nach einem Hinderniss überprüft werden soll
const int checkingSteps = 100;

// Strecke pro step
const float mmPerStep = 0.305;

//mm im Drehkreis
const int axleCircumference = 810;


int turnAngle = 0;

// nimmt mm als parameter und gibt die anzahl der steps zurück
int mmToSteps(int mm)
{
  int finalSteps = 2 * mm / mmPerStep;
  return finalSteps;
}

void driveLinearTested(unsigned int steps, bool dir, bool activated, bool schnell){

  // kommt in if wenn die Ultraschallsensoren beachtet werden sollen
  if (activated){

    // legt fest welcher der Ultraschallsensoren relevant ist
    int emergancyPin = emergencyPinBack;
    if (dir) {
      emergancyPin = emergencyPinFront;
    }

      // leuft so lange bis keine steps mehr gefahren werden sollen
    while ( steps > 0) {
      // überprüft ob der betrofene Ultraschall eine Gefahr erkennt
        if (schnell)
        {
          driveLinearFast(checkingSteps, dir);
        }        
        else
        {
          driveLinear(checkingSteps, dir);          
        }               

        steps = steps - checkingSteps;        // zieht die gerade gefahrene Steps ab
    }
  }

  //Wenn die Ultraschall Sensoren ignoriert werden sollen wird einfach die Mehtode driveLinear aufgerufen
  else {
    driveLinear(steps, dir);
  }
}


// Um nach Rechts zu fahren muss dir = false sein, um nach links zu fahren muss dir = true sein

void driveSidewardsTested(unsigned int steps, bool dir, bool activated){

  // kommt in if wenn die Ultraschallsensoren beachtet werden sollen
  if (activated){

    // legt fest welcher der Ultraschallsensoren relevant ist
    int emergancyPin = emergencyPinLeft;
    if (dir) {
      emergancyPin = emergencyPinRight;
    }

      // leuft so lange bis keine steps mehr gefahren werden sollen
    while ( steps > 0) {
      // überprüft ob der betrofene Ultraschall eine Gefahr erkennt
      if (analogRead(emergancyPin) < 800){
        driveSidewards(checkingSteps, dir);
        steps = steps - checkingSteps;        // zieht die gerade gefahrenen Steps ab

      }
      else {
        delay(1000);
      }
    }
  }

  //Wenn die Ultraschall Sensoren ignoriert werden sollen wird einfach die Mehtode driveLinear aufgerufen
  else {
    driveSidewards(steps, dir);
  }
}

//links: false
//rechts: true
void turnTested(int steps, bool dir){
  // passt turnAngle an
  /*
  if (dir)
  {
    turnAngle = turnAngle + degree;
  } else
  {
    turnAngle = turnAngle - degree;
  }
  

  if (turnAngle > 360)
  {
    turnAngle = turnAngle - 360;
  } else if (turnAngle < 0)
  {
    turnAngle = turnAngle + 360;
  }
  */
  //unsigned int steps = mmToSteps((int)((float)degree/360)*axleCircumference);
  turn(steps, dir);
}

//changing the distance of the sensor Arduino
void distanceControl(int mode) {
  if(mode == 0) {
    digitalWrite(distanceControl, LOW);       //mode 0 = emergencyDistance
  } else {
    digitalWrite(distanceControl, HIGH);      //mode 1 = tokenDistance
  }
}
